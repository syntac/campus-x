@extends('layouts.app')

@section('content')
<div class="container">
	<div class="page-title mb-2">
		<span class="h4">Accounts List</span>
        <a href="{{ route('accounts.create') }}" class="btn btn-sm btn-primary float-right">Add New</a>
	</div>
	<table class="table table-bordered bg-white shadow">
    	<thead class="thead-light">
    		<tr>
    			<th>Code</th>
                <th>Name</th>
    			<th>Account Number</th>
    			<th>Status</th>
    			<th class="text-right">Actions</th>
    		</tr>
            @forelse($data as $account)
                <tr>
                    <td>{{ $account->code }}</td>
                    <td>{{ $account->name }}</td>
                    <td>{{ $account->number }}</td>
                    <td>{{ ucfirst($account->status) }}</td>
                    <td class="text-right">
                        <a class="btn btn-primary btn-sm" href="{{ route('accounts.show', $account->id) }}">View</a>

                        <a class="btn btn-secondary btn-sm" href="{{ route('accounts.edit', $account->id) }}">Edit</a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="6">
                        <p class="alert alert-info">
                            Not yet data available. <a href="{{ route('accounts.create') }}">Add New</a>
                        </p>
                    </td>
                </tr>
            @endforelse
    	</thead>
    	<tbody>
            
    	</tbody>
    </table>

    {{ $data->links() }}

</div>
@endsection
