@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span class="h4">Create a Bank Account</span>
                    <a href="{{ route('accounts.index') }}" class="btn btn-sm btn-secondary float-right">X</a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('accounts.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="code">{{ __('Code') }}</label>
                            <input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code') }}"  placeholder="Account Code">
                            @if ($errors->has('code'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="number">{{ __('Number') }}</label>
                            <input id="number" type="text" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('number') }}"  placeholder="Account Number">
                            @if ($errors->has('number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('number') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="name">{{ __('Name') }}</label>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  placeholder="Account Name">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="status">{{ __('Status') }}</label>

                            <select name="status" id="status" class="custom-select {{ $errors->has('study') ? ' is-invalid' : '' }}">
                              <option value="" selected>Choose a status</option>
                              <option value="fix" {{ (old('status') == 'fix' ? "selected":"") }}>Fix</option>
                              <option value="open" {{ (old('status') == 'open' ? "selected":"") }}>Open</option>
                            </select>
                            @if ($errors->has('study'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Create</button> 
                            <button type="reset" class="btn btn-secondary">Reset</button> 
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
