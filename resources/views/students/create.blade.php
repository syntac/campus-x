@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span class="h4">Create a Student</span>
                    <a href="{{ route('students.index') }}" class="btn btn-sm btn-secondary float-right">X</a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('students.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name">{{ __('Student Name') }}</label>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  placeholder="Student Name">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="study">{{ __('Field of Study') }}</label>

                            <select name="study" id="study" class="custom-select {{ $errors->has('study') ? ' is-invalid' : '' }}">
                              <option value="" selected>Choose a study</option>
                              <option value="Engineering Informatics" {{ (old('study') == 'Engineering Informatics' ? "selected":"") }}>Engineering Informatics</option>
                              <option value="Computer Science" {{ (old('study') == 'Computer Science' ? "selected":"") }}>Computer Science</option>
                              <option value="Informatics Systems" {{ (old('study') == 'Informatics Systems' ? "selected":"") }}>Informatics Systems</option>
                            </select>
                            @if ($errors->has('study'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('study') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Create</button> 
                            <button type="reset" class="btn btn-secondary">Reset</button> 
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
