@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-body">
                    <h4>Student Informations</h4>
                    <table class="table table-bordered mb-0">
                        <tbody>
                            <tr>
                                <th>NIM</th>
                                <td>{{ $data->nim }}</td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{ $data->name }}</td>
                            </tr>
                            <tr>
                                <th>Study</th>
                                <td>{{ $data->study }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <h4>Invoices List: <a class="btn btn-sm btn-primary float-right" href="{{ route('invoices.create', $data->id) }}">Create an Invoice</a> </h4>
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th>Number</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Created at</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                @forelse($invoices as $invoice)
                <tbody>
                    <tr>
                        <td>{{ $invoice->number }}</td>
                        <td>Rp. {{ num2rp($invoice->amount) }}</td>
                        <td>{{ ucfirst($invoice->status) }}</td>
                        <td>{{ $invoice->created_at->format('d / M / Y') }}</td>
                        <td>
                            @if($invoice->status == 'unpaid')
                            <form class="d-inline" action="{{ route('invoices.destroy', $invoice->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this invoice?');">Delete</button>
                            </form>
                            @else
                            <button type="submit" class="btn btn-secondary btn-sm">Print</button>
                            @endif
                        </td>
                    </tr>
                </tbody>
            @empty
                <tr>
                    <td colspan="5">
                        <p class="alert alert-info">
                            This student don't have an invoice. 
                            <a href="{{ route('invoices.create', $data->id) }}">Create invoice now</a>
                        </p>
                    </td>
                </tr>
            @endforelse
            </table>
        </div>
    </div>
</div>
@endsection
