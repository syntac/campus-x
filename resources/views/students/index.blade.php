@extends('layouts.app')

@section('content')
<div class="container">
	<div class="page-title mb-2">
		<span class="h4">Students List</span>
        <a href="{{ route('students.create') }}" class="btn btn-sm btn-primary float-right">Add New</a>
	</div>
	<table class="table table-bordered bg-white shadow">
    	<thead class="thead-light">
    		<tr>
    			<th>NIM</th>
    			<th>Name</th>
    			<th>Study</th>
    			<th class="text-right">Actions</th>
    		</tr>
    	</thead>
    	<tbody>
            @forelse($data as $student)
            	<tr>
            		<td>{{ $student->nim }}</td>
            		<td>{{ $student->name }}</td>
            		<td>{{ $student->study }}</td>
            		<td class="text-right">
            			<a class="btn btn-primary btn-sm" href="{{ route('students.show', $student->id) }}">View</a>

            			<a class="btn btn-secondary btn-sm" href="{{ route('students.edit', $student->id) }}">Edit</a>
            			<form class="d-inline" action="{{ route('students.destroy', $student->id) }}" method="POST">
            				@method('DELETE')
            				@csrf
            				<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this student?');">Delete</button>
						</form>
            		</td>
            	</tr>
            @empty
                <tr>
                    <td colspan="5">
                        <p class="alert alert-info">
                            Not yet data available. <a href="{{ route('students.create') }}">Add New</a>
                        </p>
                    </td>
                </tr>
            @endforelse
    	</tbody>
    </table>

    {{ $data->links() }}

</div>
@endsection
