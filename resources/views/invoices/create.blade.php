@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span class="h4">Create an Invoice for {{ $student->name }}</span>
                    <a href="{{ route('students.show', $student->id) }}" class="btn btn-sm btn-secondary float-right">X</a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('invoices.store', $student->id) }}">
                        @csrf
                        <div class="form-group">
                            <label for="account">{{ __('Account') }}</label>

                            <select name="account" id="account" class="custom-select {{ $errors->has('account') ? ' is-invalid' : '' }}">
                                <option value="" selected>Choose an account</option>
                                @foreach ($accounts as $account)
                                    <option value="{{ $account->id }}" {{ (old('account') == $account->id ? "selected":"") }}>{{ $account->code }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('account'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('account') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="amount">{{ __('Amount') }}</label>
                            <input id="amount" type="number" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}"  placeholder="Amount invoice">
                            @if ($errors->has('amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Create</button> 
                            <button type="reset" class="btn btn-secondary">Reset</button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
