<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create default admin
        User::create([
        	'name' => 'Campus Administrator',
        	'email' => 'campus@test.app',
        	'email_verified_at' => Carbon::now(),
        	'password' => Hash::make('willy')
        ]);
    }
}
