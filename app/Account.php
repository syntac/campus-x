<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * Remove guarded fields
     */
    protected $guarded = [];
}
