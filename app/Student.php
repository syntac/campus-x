<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * Remove guarded fields
     */
    protected $guarded = [];

    /**
     * Generate NIM
     * @return string
     */
    public static function genNIM()
    {
        $lastStudent = self::orderBy('created_at', 'desc')->first();
        if (!$lastStudent) :
            $sku = 'MHSW001';
        else :
            $sku = sprintf('MHSW%03d', (int)str_replace('MHSW', '', $lastStudent->nim) + 001);
        endif;
        return $sku;
    }

    /**
     * Relation with Invoices
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
}
