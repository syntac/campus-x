<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\Account;
use App\Student;

class InvoiceController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $student_id)
    {
        $accounts   = Account::get();
        $student    = Student::find($student_id);

        return view('invoices.create', compact('accounts', 'student'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $student_id)
    {
        $student = Student::find($student_id);
        $account = Account::find($request->account);

        $request->validate([
            'account' => 'required|integer|not_in:0',
            'amount' => 'required|integer',
        ]);

        Invoice::Create([
            'number' => $account->code.$student->nim.date('ymdGis'),
            'student_id' => $student_id,
            'account_id' => $request->account,
            'amount' => $request->amount
        ]);

        return redirect()
                ->route('students.show', $student_id)
                ->with('success', 'Invoice for '.$student->name.' has been created.'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::find($id);
        $invoice->delete();

        return redirect()
                ->route('students.show', $id)
                ->with('success', 'Invoice for '.$student->name.' has been deleted.');
    }
}
