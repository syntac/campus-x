<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Invoice;
use App\Student;
use App\Account;

class APIController extends Controller
{
    /**
     * Show all invoices
     */
    public function invoices($status = false)
    {
        $data = [];

    	if($status){
    		$invoices = Invoice::where('status', $status)
    					->orderBy('created_at', 'DESC')
    					->get();

            foreach ($invoices as $invoice) {
                $student = Student::find($invoice->student_id);
                $account = Account::find($invoice->account_id);

                $data[] = [
                    'number' => $invoice->number,
                    'amount' => $invoice->amount,
                    'status' => $invoice->status,
                    'created_at' => $invoice->created_at,
                    'paid_at' => $invoice->paid_at,
                    'account' => $account,
                    'student' => $student
                ];
            }
    	} else {
    		$invoices = Invoice::orderBy('created_at', 'DESC')->get();

            foreach ($invoices as $invoice) {
                $student = Student::find($invoice->student_id);
                $account = Account::find($invoice->account_id);

                $data[] = [
                    'number' => $invoice->number,
                    'amount' => $invoice->amount,
                    'status' => $invoice->status,
                    'created_at' => $invoice->created_at,
                    'paid_at' => $invoice->paid_at,
                    'account' => $account,
                    'student' => $student
                ];
            }
    	}
    	return $data;
    }

    /**
     * Get invoice by number
     */
    public function invoice($number)
    {
    	$invoice = Invoice::where('number', $number)->first();

    	return [
    		$invoice,
    		'account' => $invoice->account,
    		'student' => $invoice->student,
    	];
    }

    /**
     * Record payment
     */
    public function payment($number)
    {
        $invoice = Invoice::where('number', $number)->first();
        $invoice->status  = 'paid';
        $invoice->paid_at = Carbon::now();
        $invoice->save();

        return $invoice;
    }
}
