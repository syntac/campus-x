<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Account::paginate(10);

        return view('accounts.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|string|unique:accounts|max:25',
            'number' => 'required|string|max:25',
            'name' => 'required|string|max:12',
            'status' => 'required|not_in:0',
        ]);

        Account::Create([
            'code' => $request->code,
            'number' => $request->number,
            'name' => $request->name,
            'status' => $request->status
        ]);

        return redirect()
                ->route('accounts.index')
                ->with('success', 'Bank Account has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Account::find($id);

        return view('accounts.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'number' => 'required|string|max:25',
            'name' => 'required|string|max:12',
            'status' => 'required|not_in:0',
        ]);

        $account = Account::find($id);
        $account->code = $request->code;
        $account->number = $request->number;
        $account->name = $request->name;
        $account->status = $request->status;
        $account->save();

        return redirect()
                ->route('accounts.index')
                ->with('success', 'Bank Account has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
