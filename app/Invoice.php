<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /**
     * Remove guarded fields
     */
    protected $guarded = [];

    /**
     * Get account detail
     */
    public function account()
    {
    	return $this->belongsTo(Account::class, 'account_id');
    }

    /**
     * Get student informations
     */
    public function student()
    {
    	return $this->belongsTo(Student::class, 'student_id');
    }
}
