<?php
Route::get('/invoices/{status?}', 'APIController@invoices');
Route::get('/invoice/{number}', 'APIController@invoice');
Route::post('/payment/{number}', 'APIController@payment');
