<?php
Auth::routes(['register' => false]);

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'StudentController@index');
	Route::get('/home', 'StudentController@index')->name('home');

    Route::resources([
	    'students' => 'StudentController',
	    'accounts' => 'AccountController',
	]);

	Route::get('/invoice/create/{student_id}', 'InvoiceController@create')->name('invoices.create');
	Route::post('/invoice/{student_id}', 'InvoiceController@store')->name('invoices.store');
	Route::delete('/invoice/{id}', 'InvoiceController@destroy')->name('invoices.destroy');
});
